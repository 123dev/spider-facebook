<?php


namespace App\Console\Commands\Crawl;


use App\Enum\CrawlStatusEnum;
use App\Models\Page;
use App\Models\Token;
use App\Service\CrawlerService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class AutoCrawl extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:auto
    {--id= : Limit page ids}
    {--token= : ID token or token string}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command auto crawl';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $id = $this->option('id');
        $token = $this->option('token');

        if ($token) {
            if ((int)$token) {
                $token = Token::where('is_active', true)
                    ->where('id', $token)
                    ->firstOrFail()
                    ->content;
            }
        } else {
            $token = Token::where('is_active', true)
                ->firstOrFail()
                ->content;
        }

        Cache::put('fb_access_token', $token);

        do {
            Page::when($id, function ($query) use ($id) {
                [$min, $max] = explode( "-", $id);
                if($min){
                    $query->where('id', '>=', $min);
                }
                if($max){
                    $query->where('id', '<=', $max);
                }
            })->where('task_crawl_status','!=', CrawlStatusEnum::RUNNING)
            ->chunk(1, function ($pages) use ($token) {
                foreach ($pages as $page) {
                    $this->info("Start crawl [$page->id][$page->name]");
                    (new CrawlerService($token))->run($page);
                }
            });

        } while(true);
    }
}