<?php

namespace App\Console\Commands;


use App\Crawler\Crawler;
use Illuminate\Console\Command;

class TestCrawl extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command test crawl';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle()
    {
        $token = "";

        $crawler = new Crawler($token);

        $feeds = $crawler->feeds(528320614043286, 'group');

        foreach ($feeds as $feed) {
            dd($feed);
        }
    }
}
