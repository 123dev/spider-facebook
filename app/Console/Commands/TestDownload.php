<?php

namespace App\Console\Commands;

use App\Crawler\Crawler;
use App\Downloader\Downloader;
use App\Jobs\DownloadAttachment;
use App\Models\Attachment;
use App\Models\Page;
use App\Service\CrawlerService;
use Illuminate\Console\Command;

class TestDownload extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:test_download';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command test crawl';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
//        1569
        $attachment = Attachment::find(1520);
        DownloadAttachment::dispatchNow($attachment);
    }
}
