<?php


namespace App\Crawler;


use App\Crawler\Fields\FeedsOfFanpageField;
use App\Crawler\Fields\FeedsOfGroupField;
use Illuminate\Support\Collection;
use Nggiahao\Facebook\Models\Feed as FB_Feed;
use GuzzleHttp\Exception\GuzzleException;
use Nggiahao\Facebook\Facebook;

class Crawler
{
    const LIMIT = 25;
    /**
     * @var Facebook Facebook SDK
     */
    protected $facebook;

    /**
     * Crawler constructor.
     *
     * @param string|null $token
     * @param null $graph_version
     */
    public function __construct(string $token = null, $graph_version = null) {
        $this->setClient(new Facebook($token, $graph_version));
    }

    /**
     * @return Facebook
     */
    public function client() {
        return $this->facebook;
    }

    /**
     * @param Facebook $facebook
     *
     * @return $this
     */
    public function setClient(Facebook $facebook) {
        $this->facebook = $facebook;
        return $this;
    }

    /**
     * @param $token
     *
     * @return Crawler
     */
    public function setToken($token) {
        $this->client()->setAccessToken($token);
        return $this;
    }

    /**
     * @param $graph_version
     */
    public function setGraphVersion($graph_version) {
        $this->client()->setGraphVersion($graph_version);
    }

    /**
     * @param string $uuid
     */
    public function feed(string $uuid) {

    }

    /**
     * @param string $uuid
     * @param string $type
     * @param int $limit
     *
     * @return Collection
     * @throws GuzzleException
     * @throws \Exception
     */
    public function feeds(string $uuid, string $type, int $limit = self::LIMIT) {
        switch ($type) {
            case 'fanpage':
                return $this->feedsOfFanpage($uuid, $limit);
            case 'group':
                return $this->feedsOfGroup($uuid, $limit);
            default:
                throw new \Exception("$type not found");
        }
    }


    /**
     * Get feed on fanpage facebook
     *
     * @param string $uuid
     * @param int $limit
     *
     * @return Collection
     * @throws \Exception|GuzzleException
     */
    public function feedsOfFanpage(string $uuid, int $limit = self::LIMIT) {
        $query = [
            'fields' => FeedsOfFanpageField::getOptionsStr(),
            'limit' => $limit,
        ];

        try {
            $response = $this->facebook->createCollectionRequest('GET', '/'.$uuid.'/post')
                ->attachQuery($query)
                ->setReturnType(FB_Feed::class)
                ->execute();

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
        return $response;
    }

    /**
     * Get feed on group facebook
     *
     * @param string $uuid
     * @param int $limit
     *
     * @return Collection
     * @throws GuzzleException
     * @throws \Exception
     */
    public function feedsOfGroup(string $uuid, int $limit = self::LIMIT) {
        $query = [
            'fields' => FeedsOfGroupField::getOptionsStr(),
            'limit' => $limit,
        ];

        try {
            $response = $this->facebook->createCollectionRequest('GET', '/'.$uuid.'/feed')
                            ->attachQuery($query)
                            ->setReturnType(FB_Feed::class)
                            ->execute();
        } catch (\Exception $e) {
            throw new \Exception( $e->getMessage() );
        }

        return $response;
    }
}