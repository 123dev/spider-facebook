<?php


namespace App\Crawler\Fields;


class AttachmentField
{
    use ToOptions;

    protected static $options = [
        'media',
        'target',
        'subattachments.limit(40)'
    ];
}
