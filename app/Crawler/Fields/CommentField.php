<?php


namespace App\Crawler\Fields;


class CommentField
{

    use ToOptions;

    protected static $options = [
        'id',
        'created_time',
        'from',
        'message',
        'message_tags',
        'attachment',
        'likes.limit(0).summary(true)',
        'comments.limit(100){id,created_time,from,message,message_tags,attachment}'
    ];
}
