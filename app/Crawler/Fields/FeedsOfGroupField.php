<?php


namespace App\Crawler\Fields;


class FeedsOfGroupField
{

    use ToOptions;

    public static $options = [
        'id',
//        'link',
        'permalink_url',
//        'name',
        'from{id,name,picture.type(large)}',
        'created_time',
        'message',
        'message_tags',
        'attachments{media,media_type,type,target,subattachments.limit(40)}',
//        'likes.limit(0).summary(true)',
        'comments.limit(200){id,created_time,from{id,name,picture.type(large)},message,message_tags,attachment,comments.limit(100){id,created_time,from{id,name,picture.type(large)},message,message_tags,attachment}}'
    ];
}
