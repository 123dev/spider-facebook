<?php


namespace App\Crawler\Fields;


/**
 * @property array  options
 */
trait ToOptions
{

    public static function getOptions() :array {
        return static::$options;
    }

    public static function getOptionsStr() :string {
        return implode(',', static::$options);
    }
}