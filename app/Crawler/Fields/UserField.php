<?php


namespace App\Crawler\Fields;


class UserField
{
    use ToOptions;

    public static $options = [
        'id',
        'permalink_url',
        'name',
        'created_time',
        'message',
        'attachments{subattachments.limit(30)}',
        'type'
    ];
}
