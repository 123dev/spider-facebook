<?php


namespace App\Downloader;


use Illuminate\Support\Arr;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Symfony\Component\Mime\MimeTypes;
use function GuzzleHttp\Psr7\stream_for;

class DownloadResult
{
    protected $content;
    protected $size;
    protected $mime;
    protected $success = false;
    protected $error = null;
    protected $headers = [];
    protected $response_code = 0;
    protected $file_name;
    protected $extension;

    /**
     * DownloadResult constructor.
     *
     * @param $content
     * @param null $error
     */
    public function __construct( $content, $error = null ) {
        $this->content = $content;
        $this->size = strlen( $content );
        if(!$error){
            $this->success = true;
        } else {
            $this->error = $error;
        }
    }

    /**
     * @return mixed
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * @return int
     */
    public function getSize(): int {
        return $this->size;
    }

    /**
     * @return mixed
     */
    public function getMime() {
        return $this->mime;
    }

    /**
     * @return mixed
     */
    public function isSuccess() {
        return $this->success;
    }

    /**
     * @return mixed
     */
    public function getError() {
        return $this->error;
    }

    /**
     * @return array
     */
    public function getHeaders(): array {
        return $this->headers;
    }

    /**
     * @return int
     */
    public function getResponseCode(): int {
        return $this->response_code;
    }

    /**
     * @return mixed
     */
    public function getFileName() {
        return $this->file_name;
    }

    /**
     * @return mixed
     */
    public function getExtension() {
        return $this->extension;
    }

    /**
     * @return StreamInterface
     */
    public function getContentStream() {
        return stream_for($this->content);
    }

    /**
     * Create DownloadResult
     *
     * @param ResponseInterface $response
     * @param string|null $error
     *
     * @return DownloadResult
     * @throws \Exception
     */
    public static function create( ResponseInterface $response, $error = null ) {
        $instance = new self($response->getBody()->getContents(), $error);
        $instance->response_code = $response->getStatusCode();

        foreach ($response->getHeaders() as $name => $values){
            $instance->headers[mb_strtolower( $name )] = $values;
        }

        if(!$error && $instance->response_code >= 200 && $instance->response_code < 300 ){
            $instance->success = true;
            $instance->resolveFileInfo();
        }

        return $instance;
    }

    /**
     * resolve file info
     *
     * @throws \Exception
     */
    protected function resolveFileInfo() {
        // mime type
        $mime_from_header = Arr::get( $this->headers, 'content-type.0', null);
        $mime_from_header = preg_replace( "/\;.*$/ui", "", $mime_from_header);

        if (!$mime_from_header) {
            $mime_from_header = (new \finfo(FILEINFO_MIME_TYPE))->buffer($this->content);
        }

        $ext = MimeTypes::getDefault()->getExtensions( $mime_from_header );

        if (empty($ext)) throw new \Exception("Can't find Ext");
        if (in_array($ext[0], ['html', 'htm'])) throw new \Exception("Can't download html file");

        $this->mime = $mime_from_header;
        $this->extension = $ext[0];
    }
}