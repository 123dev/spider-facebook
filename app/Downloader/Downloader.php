<?php


namespace App\Downloader;


use App\Libs\MakePath;
use GuzzleHttp\Client;

class Downloader
{
    /**
     * Option for Downloader
     * @var array
     */
    protected $option;
    /**
     * @var Client
     */
    protected $client;

    /**
     * Downloader constructor.
     *
     * @param array $option
     * @param array $client_option
     */
    public function __construct($option = [], $client_option = []) {
        $this->option = $option;
        $this->client = new Client($client_option);
    }

    /**
     * @param $url
     * @param $disk
     *
     * @param $type
     *
     * @param string $p
     * @param string $s
     *
     * @return object
     * @throws \Exception
     */
    public function download($url, $disk, $type, $p = '', $s = '') {
        $download_result = $this->getFromUrl($url);
        if ($download_result->isSuccess()) {
            $path = $type . '/' . MakePath::fromDateTime($download_result->getExtension(), $p, $s);
            if (\Storage::disk($disk)->put($path, $download_result->getContent())) {
                return (object) [
                    'disk' => $disk,
                    'path' => $path,
                    'size' => $download_result->getSize()
                ];
            }
        } else {
            throw new \Exception("Download Url [$url] Fail: {$download_result->getError()}");
        }
    }

    /**
     * @param $url
     * @param int $timeout
     *
     * @return DownloadResult
     */
    public function getFromUrl($url, $timeout = 480): DownloadResult {
        try {
            $response = $this->client->get($url, ['timeout' => $timeout]);

            return DownloadResult::create($response);
        } catch (\Exception $exception) {
            return new DownloadResult("", $exception->getMessage());
        }
    }

}