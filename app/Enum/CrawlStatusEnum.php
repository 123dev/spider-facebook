<?php

namespace App\Enum;


use MyCLabs\Enum\Enum;

class CrawlStatusEnum extends Enum {

    use ToOptions;

    const ERROR = -1;// chạy lỗi
    const RUNNING = 0;// đang chạy
    const DONE = 1;// chạy xong

    public static $ICON = [
        self::ERROR => "<span class='badge badge-error'>FAILED</span>",
        self::RUNNING => "<span class='badge badge-success'>RUNNING</span>",
        self::DONE => "<span class='badge badge-primary'>DONE</span>",
    ];

    public static function getIcon($status) {
        return self::$ICON[$status];
    }
}
