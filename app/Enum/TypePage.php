<?php

namespace App\Enum;


use MyCLabs\Enum\Enum;

class TypePage extends Enum {

    use ToOptions;

    CONST FANPAGE = 'FANPAGE';
    CONST GROUP = 'GROUP';

}
