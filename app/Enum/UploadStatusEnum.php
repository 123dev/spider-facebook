<?php

namespace App\Enum;


use MyCLabs\Enum\Enum;

class UploadStatusEnum extends Enum {

    use ToOptions;

    const ERROR = -1;// chạy lỗi
    const NO = 0;// không upload
    const RUNNING = 1; // đang chạy
    const PENDING = 9; // chờ duyệt
    const READY = 99; // sẵn sàng
    const DONE = 100; // đang chạy xong

    public static $ICON = [
      self::ERROR => "<span class=\"badge badge-error\">ERROR</span>",
      self::NO => "<span class=\"badge badge-error\">Không public</span>",
      self::RUNNING => "<span class=\"badge badge-primary\">RUNNING</span>",
      self::PENDING => "<span class=\"badge badge-secondary\">Chờ duyệt</span>",
      self::READY => "<span class=\"badge badge-primary\">READY</span>",
      self::DONE => "<span class=\"badge badge-success\">DONE</span>"
    ];

    public static function canRun($status){
        return in_array( $status, [
            self::READY,
        ]);
    }

    public static function getPossibleEnumValues() {
        return [
            self::$ICON[self::NO] => self::NO,
            self::$ICON[self::PENDING] => self::PENDING,
            self::$ICON[self::READY] => self::READY,
            self::$ICON[self::DONE] => self::DONE,
        ];
    }

    public static function getIcon($status) {
        return self::$ICON[$status];
    }

}
