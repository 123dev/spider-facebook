<?php

namespace App\Http\Controllers\Admin;

use App\Enum\UploadStatusEnum;
use App\Http\Requests\CommentRequest;
use App\Models\Comment;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CommentCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CommentCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
//    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
//    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
//    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        $this->crud->setModel(\App\Models\Comment::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/comment');
        $this->crud->setEntityNameStrings('comment', 'comments');
        $this->crud->orderBy('fb_created_time', 'desc');
        $this->crud->addClause('with', 'attachments');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumns([
            [
                'name' => 'id',
                'label' => '#',
            ],
            [
                'name' => 'code',
                'label' => 'Code',
            ],
            [
                'label'     => 'Parent',
                'type'      => 'select',
                'name'      => 'parent_id',
                'entity'    => 'parent',
                'attribute' => 'id',
                'model'     => Comment::class,
            ],
            [
                'name' => 'message',
                'label' => 'Content',
                'type' => 'text_box',
            ],
            [
                'name' => 'attachments',
                'label' => 'Attachments',
                'type' => 'preview_image',
                'limit' => 2,
                'col' => 'col-6'
            ],
            [
                'name' => 'fb_created_time',
                'label' => "FB created at",
                'type' => 'closure',
                'function' => function ($entry) {
                    return "<a href='$entry->url' target='_blank'>$entry->fb_created_time</a>";
                },
            ],
            [
                'name' => 'task_upload_status',
                'label' => "Status",
                'type' => 'closure',
                'function' => function ($entry) {
                    $value = $entry->task_upload_status;
                    return UploadStatusEnum::getIcon($value);
                },
            ],
        ]);
    }


    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
//        $this->setupCreateOperation();
    }
}
