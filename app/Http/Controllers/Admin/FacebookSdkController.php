<?php


namespace App\Http\Controllers\Admin;


use App\Crawler\Crawler;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;

class FacebookSdkController extends Controller
{
    /**
     * @param Request $request
     * @param Crawler $crawler
     *
     * @return mixed
     * @throws GuzzleException
     */
    public function index(Request $request, Crawler $crawler) {
        $type = $request->get('type');
        $uuid = $request->get('id_fb');
        $access_token = $request->get('access_token');

        $crawler->setToken($access_token);
        
        $feeds = [];
        if ($type && $uuid) {
            switch ($type) {
                case "GROUP":
                    $feeds = $crawler->feedsOfGroup($uuid);
                    break;
                case "FANPAGE":
                    $feeds = $crawler->feedsOfFanpage($uuid);
                    break;
            }
        } else {
            $feeds = [];
        }
        
        return view("FacebookSDK.index", compact('feeds'));
    }
}