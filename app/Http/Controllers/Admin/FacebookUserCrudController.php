<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\FacebookUserRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class FacebookUserCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class FacebookUserCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        $this->crud->setModel(\App\Models\FacebookUser::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/facebook_user');
        $this->crud->setEntityNameStrings('facebook_user', 'facebook_users');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumns([
            [
                'name' => 'id',
                'label' => '#',
            ],
            [
                'name' => 'id_fb',
                'label' => 'Code',
            ],
            [
                'name' => 'name',
                'label' => "Name",
                'type' => 'closure',
                'function' => function ($entry) {
                    return "<img class='img-avatar mr-2' style='width: 35px' src='{$entry->avatar}'>
                            <a href='{$entry->link_facebook}'><strong>{$entry->name}</strong></a>";
                },
            ],
        ]);
    }
}
