<?php

namespace App\Http\Controllers\Admin;

use App\Enum\UploadStatusEnum;
use App\Http\Requests\FeedRequest;
use App\Models\Feed;
use App\Models\Page;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;

/**
 * Class FeedCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class FeedCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
//    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
//    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     * @throws \Exception
     */
    public function setup()
    {
        $this->crud->setModel(\App\Models\Feed::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/feed');
        $this->crud->setEntityNameStrings('feed', 'feeds');
        $this->crud->orderBy('fb_created_time', 'desc');
        $this->crud->addClause('with', 'attachments');
        $this->crud->enableDetailsRow();

    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumns([
            [
                'name' => 'id',
                'label' => '#',
            ],
            [
                'name' => 'code',
                'label' => 'Code',
            ],
            [
                'name' => 'message',
                'label' => 'Content',
                'type' => 'text_box',
            ],
            [
                'name' => 'attachments',
                'label' => 'Attachments',
                'type' => 'preview_image',
                'limit' => 2,
                'col' => 'col-6'
            ],
            [
                'name' => 'fb_created_time',
                'label' => "FB created at",
                'type' => 'closure',
                'function' => function ($entry) {
                    return "<a href='$entry->url' target='_blank'>$entry->fb_created_time</a>";
                },
            ],
            [
                'name' => 'task_upload_status',
                'label' => "Status",
                'type' => 'closure',
                'function' => function ($entry) {
                    $value = $entry->task_upload_status;
                    return UploadStatusEnum::getIcon($value);
                },
            ],
        ]);

        $this->crud->addFilter([
            'type'  => 'date_range',
            'name'  => 'from_to',
            'label' => 'Time create'
        ], false,
        function ($value) {
            $dates = json_decode($value);
            $this->crud->addClause('where', 'fb_created_time', '>=', $dates->from);
            $this->crud->addClause('where', 'fb_created_time', '<=', $dates->to . ' 23:59:59');
        });

        $this->crud->addFilter([
            'name' => 'filter_page_id',
            'type' => 'select2_ajax',
            'label'=> 'Page',
            'placeholder' => 'Pick a Page'
        ], url('admin/ajax-page-options'), // the ajax route
        function($value) {
            if($value) { //Bug's backpack
                $this->crud->addClause('where', 'page_id', $value);
            }
        });

        $this->crud->addFilter([
            'type' => 'dropdown',
            'name' => 'filter_status',
            'label' => 'Lọc theo trạng thái',
        ], UploadStatusEnum::toOptions(),
        function ($value) {
            $this->crud->addClause('where', 'task_upload_status', $value);
        });



    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
//        $this->setupCreateOperation();
    }

    public function pageOptions(Request $request)
    {
        $term = $request->input('term');
        return Page::where('name', 'like', '%'.$term.'%')->get()->pluck('name', 'id');
    }

    /**
     * @param $id
     *
     */
    public function showDetailsRow( $id ) {
        $this->crud->hasAccessOrFail('list');

        $id = $this->crud->getCurrentEntryId() ?? $id;

        $feed = Feed::with('attachments')->find($id);

        return view(backpack_view('crud.columns.feed_row'), compact('feed'));

    }
}
