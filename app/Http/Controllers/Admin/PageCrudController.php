<?php

namespace App\Http\Controllers\Admin;

use App\Enum\CrawlStatusEnum;
use App\Enum\TypePage;
use App\Http\Requests\PageRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PageCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PageCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
//    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     * @throws \Exception
     */
    public function setup()
    {
        $this->crud->setModel(\App\Models\Page::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/page');
        $this->crud->setEntityNameStrings('page', 'pages');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumns([
            [
                'name' => 'id',
                'label' => '#'
            ],
            [
                'name' => 'name',
                'label' => 'Name',
                'type' => 'closure',
                'function' => function($entry) {
                    return "<a href='$entry->url' target='_blank'>$entry->name</a>";
                }
            ],
            [
                'name' => 'type',
                'label' => "Type",
                'type' => 'closure',
                'function' => function($entry) {
                    return TypePage::search($entry->type);
                }
            ],
            [
                'name' => 'task_crawl_status',
                'label' => "Status",
                'type' => 'closure',
                'function' => function($entry) {
                    $value = $entry->task_crawl_status;
                    return CrawlStatusEnum::getIcon($value);
                }
            ],
            [
                'name' => 'latest_time_crawl',
                'label' => "Last time crawl",
                'type' => 'text',
            ],
            [
                'name' => 'is_active',
                'label' => "Active",
                'type' => 'check',
            ]
        ]);


        $this->crud->addFilter([
            'name' => 'Type',
            'type' => 'dropdown',
            'label'=> 'Type'
        ], function() {
            return TypePage::toOptions();
        }, function($value) {
            $this->crud->addClause('where', 'type', $value);
        });
        $this->crud->addFilter([
            'name' => 'filter_status',
            'type' => 'dropdown',
            'label'=> 'Status'
        ], CrawlStatusEnum::toOptions(), function($value) { // if the filter is active
            $this->crud->addClause('where', 'task_crawl_status', $value);
        });

    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {

        $this->crud->setValidation(PageRequest::class);

        CRUD::addFields([
            [
                'name'        => 'is_active',
                'label'       => "Active",
            ],
            [
                'name' => 'id_fb',
                'label' => "ID on Facebook",
                'type' => 'text',
                'wrapper'   => [
                    'class'      => 'form-group col-md-6'
                ]
            ],
            [
                'name' => 'url',
                'label' => "Url Facebook",
                'type' => 'text',
                'wrapper'   => [
                    'class'      => 'form-group col-md-6'
                ]
            ],
            [
                'name' => 'name',
                'label' => "Name",
                'type' => 'text',
                'wrapper'   => [
                    'class'      => 'form-group col-md-6'
                ]
            ],
            [
                'name'        => 'type',
                'label'       => "Type",
                'type'        => 'select_from_array',
                'options'     => TypePage::toOptions(),
                'allows_null' => false,
                'wrapper'   => [
                    'class'      => 'form-group col-md-6'
                ]
            ],

        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
        CRUD::addField([
            'name'        => 'task_crawl_status',
            'label'       => "Status",
            'type'        => 'select_from_array',
            'options'     => CrawlStatusEnum::toOptions(),
            'allows_null' => false,
            'wrapper'   => [
                'class'      => 'form-group col-md-6'
            ]
        ]);
    }
}
