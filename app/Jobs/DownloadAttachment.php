<?php

namespace App\Jobs;

use App\Models\Attachment;
use App\Models\Token;
use App\Service\AttachmentManager;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;

class DownloadAttachment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $attachment;

    /**
     * Create a new job instance.
     *
     * @param Attachment $attachment
     */
    public function __construct(Attachment $attachment)
    {
        $this->attachment = $attachment;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle() {
        if (Cache::has('fb_access_token')) {
            $token = Cache::get('fb_access_token');
        } else {
            $token = Token::where('is_active', true)
                ->firstOrFail()
                ->content;
            Cache::put('fb_access_token', $token);
        }
        AttachmentManager::download($this->attachment, $token);
    }
}
