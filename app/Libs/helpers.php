<?php

/**
 * Returns the params from a URL in the form of an array.
 *
 * @param string $url
 *
 * @return array
 */
if (!function_exists('get_params')) {
    function get_params($url)
    {
        $query = parse_url($url, PHP_URL_QUERY);
        if (!$query) {
            return [];
        }
        parse_str($query, $params);

        return $params;
    }
}

if (!function_exists('remove_params')) {
    /**
     * Remove params from a URL.
     *
     * @param string $url
     * @param array $params_filter
     * @return string
     */
    function remove_params($url, array $params_filter = []) {

        $parts = parse_url($url);

        $query = '';
        if (isset($parts['query']) && !empty($params_filter)) {
            $params = [];
            parse_str($parts['query'], $params);

            // Remove query params
            foreach ($params_filter as $param_name) {
                unset($params[$param_name]);
            }

            if (count($params) > 0) {
                $query = '?' . http_build_query($params, null, '&');
            }
        }

        $scheme = isset($parts['scheme']) ? $parts['scheme'] . '://' : '';
        $host = isset($parts['host']) ? $parts['host'] : '';
        $port = isset($parts['port']) ? ':' . $parts['port'] : '';
        $path = isset($parts['path']) ? $parts['path'] : '';
        $fragment = isset($parts['fragment']) ? '#' . $parts['fragment'] : '';

        return $scheme . $host . $port . $path . $query . $fragment;
    }
}

function replace_heading($html) {
    $html = preg_replace('/<h[1-6]>/', '<p style="font-weight:bold">', $html);
    $html = preg_replace('/<\/h[1-6]>/', '</p>', $html);
    $html = preg_replace('/<\/p>\s*<br(.*)>/', '</p>', $html);
    return $html;
}