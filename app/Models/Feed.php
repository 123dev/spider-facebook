<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use GrahamCampbell\Markdown\Facades\Markdown;
use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'feeds';

    protected $guarded = ['id'];

    protected $casts = [
        'fb_created_time' => 'datetime'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getMessageAttribute($value) {
        if (empty($value)) return null;
        return (replace_heading(nl2br(Markdown::convertToHtml($value))));
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function page() {
        return $this->belongsTo(Page::class, 'page_id');
    }

    public function comments() {
        return $this->hasMany(Comment::class, 'feed_id');
    }

    public function root_comments() {
        return $this->comments()->where('parent_id', null);
    }

    public function attachments() {
        return $this->morphMany(Attachment::class, 'attachmentable');
    }

    public function user() {
        return $this->belongsTo(FacebookUser::class, 'user_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
