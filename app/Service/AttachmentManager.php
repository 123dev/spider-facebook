<?php


namespace App\Service;


use App\Downloader\Downloader;
use App\Enum\DownloadStatusEnum;
use App\Jobs\DownloadAttachment;
use App\Models\Attachment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Nggiahao\Facebook\Facebook;
use \Nggiahao\Facebook\Models\Attachment as FB_Attachment;

class AttachmentManager
{

    /**
     * @param $model
     * @param Collection|array $attachments_node
     */
    public static function store($model, $attachments_node) {
        /** @var FB_Attachment $attachment_node */
        foreach ($attachments_node as $attachment_node) {
            if (empty($attachment_node)) continue;

            if (!$attachment_node->id) {
                $url = remove_params(get_params($attachment_node->src)['url']);
                $id_fb = hash('sha256', $url);
            } else {
                $id_fb = $attachment_node->id;
            }

            if (!Attachment::where('id_fb', $id_fb)->exists()) {
                $attachment = $model->attachments()->create([
                    'id_fb' => $id_fb,
                    'url' => $attachment_node->src,
                    'type' => $attachment_node->type,
                ]);
                DownloadAttachment::dispatch($attachment);
            }

        }
    }

    /**
     * @param Attachment $attachment
     * @param $access_token
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function download(Attachment $attachment, $access_token) {
        $downloader = new Downloader();
        $facebook_sdk = new Facebook();
        $facebook_sdk->setAccessToken($access_token);

        $type = $attachment->type;
        $disk = 'facebook_storage';
        try {
            switch ($type) {
                case 'video':
                case 'video_inline':
                    $response = $facebook_sdk->createRequest(
                        'GET',
                        '/'.$attachment->id_fb)
                        ->attachQuery([
                            'fields' => 'source,length',
                        ])->execute();
                    $video = $response->getBody();
                    $download_result = $downloader->download($video['source'], $disk, $type, '', $attachment->id);

                    $attachment->update([
                        'url' => $video['source'],
                        'file_name' => $download_result->path,
                        'disk' => $download_result->disk,
                        'info' => [
                            'size' => $download_result->size,
                            'length' => $video['length'],
                        ],
                        'download_status' => DownloadStatusEnum::DONE,
                    ]);
                    break;
                case 'animated_image_share':
                case 'image_share':
                case 'sticker':
                    $params = get_params($attachment->url);
                    if (!empty($params['url'])) {
                        $url = remove_params($params['url']);
                    } else {
                        $url = $attachment->url;
                    }
                    $download_result = $downloader->download($url, $disk, $type, '', $attachment->id);
                    $attachment->update([
                        'url' => $url,
                        'file_name' => $download_result->path,
                        'disk' => $download_result->disk,
                        'info' => [
                            'size' => $download_result->size,
                        ],
                        'download_status' => DownloadStatusEnum::DONE,
                    ]);

                    break;
                case 'photo':
                    $response = $facebook_sdk->createRequest(
                        'GET',
                        '/'.$attachment->id_fb)
                        ->attachQuery(['fields' => 'images'])
                        ->execute();

                    $response = $response->getBody();
                    $image = ($response['images'][0]);
                    $download_result = $downloader->download($image['source'], $disk, $type, '', $attachment->id);

                    $attachment->update([
                        'url' => $image['source'],
                        'file_name' => $download_result->path,
                        'disk' => $download_result->disk,
                        'info' => [
                            'size' => $download_result->size,
                            'width' => $image['width'],
                            'height' => $image['height'],
                        ],
                        'download_status' => DownloadStatusEnum::DONE,
                    ]);
                    break;
                default:
                    $attachment->download_status = DownloadStatusEnum::ERROR;
                    $attachment->save();
                    \Log::error("Can download attachment [$attachment->id] type $type");
            }
        } catch (\Exception $exception) {
            \Log::error($exception->getMessage());
            $attachment->download_status = DownloadStatusEnum::ERROR;
            $attachment->save();
        }
    }
}