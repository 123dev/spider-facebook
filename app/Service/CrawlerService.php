<?php


namespace App\Service;


use App\Crawler\Crawler;
use App\Enum\CrawlStatusEnum;
use App\Enum\TypePage;
use App\Models\Page;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Collection;
use Vuh\CliEcho\CliEcho;

class CrawlerService
{
    protected $access_token;

    protected $id_fb;
    protected $type;
    protected $limit;

    protected $time_run;

    /**
     * Handle Crawl constructor.
     *
     * @param $access_token
     */
    public function __construct ( $access_token ) {
        $this->access_token = $access_token;
    }

    /**
     * Luồng chạy crawl chính
     *
     * @param Page $page
     * @param int $limit
     *
     * @return void
     * @throws GuzzleException
     */
    public function run(Page $page, int $limit = 15) {
        $this->time_run = now();
        $this->limit = $limit;

        $page->latest_time_crawl = $this->time_run;
        $page->setStatusCrawl(CrawlStatusEnum::RUNNING);
        $page->save();

        $this->type = $page->type;
        $this->id_fb = $page->id_fb;
        \Log::alert("Start Crawl: ($this->type) [{$page->id}][$this->id_fb]");

        $feeds = $this->getInfo();
        foreach ($feeds as $feed) {
            $feed = FeedGraphManager::store($page, $feed);
            CliEcho::infonl("[$feed->id_fb] [$feed->url]");
        }

        $page->setStatusCrawl(CrawlStatusEnum::DONE);
        $page->save();
//        try {
//
//        } catch (\Exception $exception) {
//
//            \Log::error("Crawl Fail ($this->type) [{$page->id}][{$this->id_fb}]: {$exception->getMessage()}");
//            CliEcho::errornl("Crawl Fail ($this->type) [{$page->id}][{$this->id_fb}]: {$exception->getMessage()}");
//
//            $page->setStatusCrawl(CrawlStatusEnum::ERROR);
//            $page->save();
//
//            return;
//        }
    }

    /**
     *  Lấy data từ facebook
     *
     * @return Collection
     * @throws GuzzleException
     * @throws \Exception
     */
    public function getInfo() {
        $crawler = new Crawler($this->access_token);

        switch ($this->type) {
            case TypePage::GROUP:
                $feeds = $crawler->feedsOfGroup($this->id_fb, $this->limit);
                break;
            case TypePage::FANPAGE:
                $feeds = $crawler->feedsOfFanpage($this->id_fb, $this->limit);
                break;
            default:
                throw new \Exception("Not support for type: {$this->type}");
        }

        return $feeds;
    }
}