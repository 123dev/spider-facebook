<?php


namespace App\Service;


use App\Models\Comment;
use App\Models\FacebookUser;
use App\Models\Feed;
use Nggiahao\Facebook\Models\Feed as FB_Feed;
use App\Models\Page;
use Nggiahao\Facebook\Models\User as FB_User;
use Nggiahao\Facebook\Models\Comment as FB_Comment;

class FeedGraphManager
{
    /**
     * @param Page $page
     * @param FB_Feed $feed_node
     *
     * @return Feed
     */
    public static function store(Page $page, FB_Feed $feed_node) {
        $user = self::storeUser($feed_node->from);

        /** @var Feed $feed */
        $feed = $page->feeds()->firstOrCreate([
            'id_fb' => $feed_node->id,
        ],[
            'url' => $feed_node->permalink_url,
            'link' => $feed_node->link,
            'user_id' => $user->id,
            'message' => $feed_node->message,
            'fb_created_time' => $feed_node->created_time,
        ]);
        AttachmentManager::store($feed, $feed_node->attachments);

        foreach ($feed_node->comments ?? [] as $comment_node) {
            /** @var FB_Comment $comment_node */
            $comment = self::storeComment($feed, null, $comment_node);
            AttachmentManager::store($comment, [$comment_node->attachment]);

            foreach ($comment_node->comments ?? [] as $sub_comment_node) {
                /** @var FB_Comment $sub_comment_node */
                $sub_comment = self::storeComment($feed, $comment->id, $sub_comment_node);
                AttachmentManager::store($sub_comment, [$sub_comment_node->attachment]);
            }
        }

        return $feed;
    }

    /**
     * @param FB_User $user
     *
     * @return FacebookUser
     */
    public static function storeUser(FB_User $user) {
        return FacebookUser::firstOrCreate(
            [
                'id_fb' => $user->id,
            ],[
                'name' => $user->name,
                'avatar' => $user->avatar,
            ]
        );
    }

    /**
     * @param Feed $feed
     * @param $parent_id
     * @param FB_Comment $comment_node
     *
     * @return Comment
     */
    public static function storeComment(Feed $feed, $parent_id, FB_Comment $comment_node) {
        $user = self::storeUser($comment_node->from);

        return Comment::firstOrCreate([
            'id_fb' => $comment_node->id,
        ],[
            'user_id' => $user->id,
            'message' => $comment_node->message,
            'feed_id' => $feed->id,
            'parent_id' => $parent_id,
            'fb_created_time' => $comment_node->created_time,
        ]);
    }


}