<?php

return [
    "graph_version" => "v7.0",
    "default_access_token" => env("DEFAULT_TOKEN_FB", null)
];