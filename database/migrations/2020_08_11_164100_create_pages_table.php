<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->id();
            $table->string('id_fb')->index()->comment('ID Facebook');
            $table->string('url')->nullable()->comment('Link Facebook');
            $table->string('name');
            $table->string( 'type')->index()->comment( 'See App\Enum\TypePage');

            $table->integer('task_crawl_status')->default(1)->comment('(-1).error - (0).running - (1).done');
            $table->dateTime('latest_time_crawl')->nullable();
            $table->boolean( 'is_active')->default(0)->comment('on/off');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
