<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feeds', function (Blueprint $table) {
            $table->id();
            $table->string( 'code')->nullable()->comment( 'Code từ hệ thống chính')->index();
            $table->string('id_fb')->index()->comment('ID Facebook');
            $table->string('url')->nullable()->comment('Link Facebook');

            $table->string('link')->nullable()->comment('backlink đến trang khác, link video, link dến ảnh feed');

            $table->unsignedInteger('page_id')->index();
            $table->unsignedBigInteger('user_id')->index();
            $table->text('message')->nullable();
            $table->integer('like')->default(0);

            $table->dateTime('fb_created_time');
            $table->integer('task_upload_status')
                ->default(9)
                ->comment('0.No/1.Running/9.Pending/99.Ready/100.Done/-1.False');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feeds');
    }
}
