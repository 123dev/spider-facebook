<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attachments', function (Blueprint $table) {
            $table->id();
            $table->string( 'code')->nullable()->comment( 'Code từ hệ thống chính')->index();
            $table->string('id_fb')->index()->comment('ID Facebook');
            $table->text('url')->nullable();

            $table->unsignedBigInteger('attachmentable_id');
            $table->string('attachmentable_type');

            $table->string('type', 50)->index();
            $table->string('file_name')->nullable();
            $table->string('disk')->nullable();

            $table->json('info')->nullable();

            $table->integer('download_status')->default(0)->comment('-1.False - 0.Ready - 1-Done');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attachments');
    }
}
