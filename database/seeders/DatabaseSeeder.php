<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        if ( config( 'app.env' ) == 'local' ) {
            \App\Models\User::firstOrCreate( [
                'email' => 'admin@admin.com',
            ],
                [
                    'name'     => 'Administrator',
                    'password' => bcrypt( 'password' ),
                ] );

            \App\Models\Page::firstOrCreate([
                'id_fb' => '528320614043286'
            ], [
                'url' => 'https://www.facebook.com/groups/hoinguoidammenhiepanh',
                'name' => 'Hội Đam Mê Nhiếp Ảnh - Aphoto',
                'type' => \App\Enum\TypePage::GROUP,
                'is_active' => true
            ]);
        }
    }
}
