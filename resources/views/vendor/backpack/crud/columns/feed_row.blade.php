<div class="row" style="white-space: normal">
    <div class="col-7">
        <div>
            @if($feed->page->type === \App\Enum\TypePage::GROUP)
                <img class="img-avatar mr-2" style="width: 35px" src="{{$feed->user->avatar}}">
                <a href="{{"https://www.facebook.com/".$feed->user->id_fb}}">
                    <strong>{{$feed->user->name}}</strong></a>
            @endif
        </div>
        <p class="small mt-2"><a href="{{$feed->url}}">{{$feed->fb_created_time}}</a></p>
        @if($feed->message)
            {!! ($feed->message) !!}
        @endif
        <div style="column-count: 4">
            @foreach($feed->attachments as $attachment)
                <a data-fancybox="gallery" href="{{$attachment->getUrl()}}" class="d-block mb-2 w-100">
                    <img src="{{$attachment->getUrl()}}" style="max-width: 100%;height: auto;display: block;vertical-align: middle;">
                </a>
            @endforeach
        </div>
    </div>
    <div class="col-5" style="max-height: 500px; overflow: scroll">
        @foreach($feed->root_comments as $comment)
            <div class="mt-2">
                <img class="img-avatar mr-2" style="width: 35px" src="{{$comment->user->avatar}}">
                <span><a href="{{"https://www.facebook.com/".$comment->user->id_fb}}"><strong>{{$comment->user->name}}</strong></a> {{$comment->fb_created_time}}</span>
                <div class="row" style="padding-left: 53px">
                    <div class="col-12">
                        @if($comment->message)
                            {!! ($comment->message) !!}
                        @endif
                    </div>
                    @foreach($comment->attachments as $attachment)
                    <div class="col-4">
                        <a data-fancybox="gallery" href="{{$attachment->getUrl()}}">
                            <img src="{{$attachment->getUrl()}}" class="img-fluid">
                        </a>
                    </div>
                    @endforeach
                    @foreach($comment->children as $sub_comment)
                        <div class="col-12">
                            <img class="img-avatar mr-2" style="width: 35px" src="{{$sub_comment->user->avatar}}">
                            <span><a href="{{"https://www.facebook.com/".$sub_comment->user->id_fb}}"><strong>{{$sub_comment->user->name}}</strong></a> {{$sub_comment->fb_created_time}}</span>
                            <div class="row" style="padding-left: 53px">
                                <div class="col-12">
                                    @if($sub_comment->message)
                                        {!! ($sub_comment->message) !!}
                                    @endif
                                </div>
                                @foreach($sub_comment->attachments as $attachment)
                                    <div class="col-4">
                                        <a data-fancybox="gallery" href="{{$attachment->getUrl()}}">
                                            <img src="{{$attachment->getUrl()}}" class="img-fluid">
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endforeach
    </div>
</div>