@php
    /** @var $entry */
    /** @var $column */

    $images =json_decode($entry->attachments);
    $limit = array_key_exists('limit', $column) ? $column['limit'] : 40;
    $count_image = 0;
@endphp
<div class="row" id="">
    @if($images !== null && count($images))
        @foreach($images as $image)
            <div class="{{array_key_exists('col', $column) ? $column['col'] : 'col-3 mb-3'}}" style="height: 250px">
                <a data-fancybox="gallery" href="{{\Storage::disk($image->disk)->url($image->file_name)}}">
                    <img src="{{\Storage::disk($image->disk)->url($image->file_name)}}" alt="" style="width: 100%; height: 100%; object-fit: cover;">
                </a>
            </div>
            @if(++$count_image >= $limit)
                @break
            @endif
        @endforeach
    @else
        <span class="text-error">Not image preview</span>
    @endif
</div>

