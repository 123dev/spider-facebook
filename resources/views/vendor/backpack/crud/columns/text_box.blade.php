{{-- regular object attribute --}}
@php
    $value = data_get($entry, $column['name']);
    $column['text'] = is_string($value) ? $value : '';
@endphp

<div class="" style="display: block;
    max-width: 300px;
    white-space: pre-wrap;
    max-height: 300px;
    overflow: hidden">
    {!! $column['text'] !!}
</div>

