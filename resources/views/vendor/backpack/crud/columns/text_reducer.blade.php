@php
    $value = data_get($entry, $column['name']);
    $column['limit'] = $column['limit'] ?? 40;
    $column['text'] = \App\Libs\TextReducer::text($value, $column['limit'])
@endphp

<span>
    {{ $column['text'] }}
</span>