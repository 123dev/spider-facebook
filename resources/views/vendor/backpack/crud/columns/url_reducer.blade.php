@php
    $value = data_get($entry, $column['name']);
    $column['limit'] = $column['limit'] ?? 40;
    $column['text'] = \App\Libs\TextReducer::url($value, $column['limit'])
@endphp
<a href="{{$value}}" target="_blank">{{ $column['text'] }}</a>